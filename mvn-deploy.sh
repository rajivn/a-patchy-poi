#! /bin/sh
VERSION="3.7-PATCHED"
ARTIFACTID="apatchypoi"

mvn install:install-file \
    -DartifactId=$ARTIFACTID \
    -Dversion=$VERSION \
    -DgroupId=com.chalktips \
    -Dpackaging=jar \
    -Dfile=target/$ARTIFACTID.jar \
    -DpomFile=$ARTIFACTID.pom
