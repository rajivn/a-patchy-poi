package com.chalktips.apatchy.poi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.hwpf.usermodel.Range;

public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Please provide a filename to parse.");
			System.exit(1);
		}

		try {

			System.out.println("Parsing: " + args[0]);
			HWPFDocument doc = new HWPFDocument(new FileInputStream(args[0]));
			Range r = doc.getRange();
			for (int i = 0; i < r.numParagraphs(); i++) {
				Paragraph p = r.getParagraph(i);
				ParagraphProperties pap = p.cloneProperties();
				System.out.println("Value of fBidi is: " + PatchingHelper.getFBidi(pap));
			}
			System.out.println("Finished Parsing");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

}
