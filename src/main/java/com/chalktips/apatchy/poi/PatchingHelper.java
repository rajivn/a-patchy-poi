package com.chalktips.apatchy.poi;

import org.apache.poi.hwpf.usermodel.ParagraphProperties;

public class PatchingHelper {

	/**
	 * This method is a place holder for a pointcut that will be used to inject
	 * the value of fBidi into the PAP.
	 * 
	 * @param pap
	 * @return
	 */
	public static byte getFBidi(ParagraphProperties pap) {
		return 0;
	}

}
