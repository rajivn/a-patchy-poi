package org.apache.poi.hwpf.sprm;

import org.apache.poi.hwpf.usermodel.ParagraphProperties;

public aspect Patch {

	// ~ Private Properties ==========================================

	private static final int FBIDI_OPCODE = 0x41;

	private byte ParagraphProperties.fBidi = (byte) 0x0;

	// ~ Public Methods ==============================================

	public byte ParagraphProperties.getFBidi() {
		return this.fBidi;
	}

	public void ParagraphProperties.setFBidi(byte fBidi) {
		this.fBidi = fBidi;
	}

	// ~ Pointcuts ===================================================

	/**
	 * Pointcut that detects a call to
	 * {@link org.apache.poi.hwpf.sprm.ParagraphSprmUncompressor#unCompressPAPOperation(ParagraphProperties, SprmOperation)
	 * unCompressPAPOperation}
	 * 
	 * @param newPAP
	 * @param sprm
	 */
	pointcut papOperation(ParagraphProperties newPAP, SprmOperation sprm):
		call(static void unCompressPAPOperation(ParagraphProperties, SprmOperation)) &&
		args(newPAP, sprm);

	/**
	 * This pointcut intercepts a call to get the fBidi property of a
	 * paragraphProperty
	 * 
	 * @param pap
	 */
	pointcut accessFBidi(ParagraphProperties pap):
		within(com.chalktips.apatchy.poi.*) &&
		args(pap) &&
		call(public static byte *.getFBidi(ParagraphProperties));

	// ~ Advices =====================================================

	/**
	 * Advice that wraps <code>around</code>
	 * {@link #operation(ParagraphProperties, SprmOperation)} pointcut to detect
	 * text direction.
	 */
	void around(ParagraphProperties newPAP, SprmOperation sprm): papOperation(newPAP, sprm) {
		if (sprm.getOperation() == FBIDI_OPCODE) {
			newPAP.setFBidi((byte) sprm.getOperand());
			return;
		}
		proceed(newPAP, sprm);
	}

	/**
	 * Returns the fBidi property as set by this aspect.
	 */
	byte around(ParagraphProperties pap): accessFBidi(pap) {
		return pap.getFBidi();
	}
}
