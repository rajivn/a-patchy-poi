package com.chalktips.apatchy.poi;

import java.io.IOException;
import java.io.InputStream;

import junit.framework.TestCase;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.hwpf.usermodel.Range;
import org.junit.Test;

import com.chalktips.apatchy.poi.PatchingHelper;

public class PatchTests extends TestCase {

	@Test
	public void testPass() {
		assertTrue(true);
	}

	@Test
	public void testFBidiIsSet() throws IOException {
		InputStream is = getClass().getClassLoader().getResourceAsStream("rtl.doc");
		HWPFDocument doc = new HWPFDocument(is);
		Range r = doc.getRange();
		for (int i = 0; i < r.numParagraphs(); i++) {
			ParagraphProperties pap = r.getParagraph(i).cloneProperties();
			assertEquals(PatchingHelper.getFBidi(pap), 1);
		}
	}
}
